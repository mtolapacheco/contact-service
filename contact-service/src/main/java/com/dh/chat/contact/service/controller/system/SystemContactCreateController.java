package com.dh.chat.contact.service.controller.system;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.chat.contact.api.model.Contact;
import com.dh.chat.contact.service.command.SystemContactCreateCmd;
import com.dh.chat.contact.service.controller.Constants;
import com.dh.chat.contact.service.model.impl.ContactBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author marvin tola
 */
@Api(
        tags= Constants.ContactTag.NAME,
        description=Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SYSTEM_CONTACTS)
@RequestScope
public class SystemContactCreateController {


    @Autowired
    private SystemContactCreateCmd systemContactCreateCmd;

    @ApiOperation(
            value="Create a contact"
    )
    @RequestMapping(method = RequestMethod.POST)
    public Contact createContact(@RequestBody SystemContactCreateInput input){
        //throw new UnsupportedOperationException("Implementacion is pending");

        systemContactCreateCmd.setInput(input);
        systemContactCreateCmd.execute();

        return ContactBuilder.getInstance(systemContactCreateCmd.getContact()).build();
    }
}
