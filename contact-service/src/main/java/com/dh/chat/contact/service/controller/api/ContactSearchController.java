package com.dh.chat.contact.service.controller.api;

import com.dh.chat.contact.api.input.ContactSearchInput;
import com.dh.chat.contact.service.commons.Pagination;
import com.dh.chat.contact.service.controller.Constants;
import com.dh.chat.contact.service.model.domain.Contact;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author marvin tola
 */

@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SECURE_CONTACTS)
@RequestScope
public class ContactSearchController {

    @ApiOperation(
            value = "Create a contact single"
    )

    @RequestMapping(
            value = "/search",
            method = RequestMethod.POST)
    public Pagination<Contact> searchContact(@RequestHeader("Account-ID")Long accountId,
                                             @RequestHeader("User-ID")Long userId,
                                             @RequestParam("limit")Integer limit,
                                             @RequestParam("page")Integer page,
                                             @RequestBody ContactSearchInput input){
        throw new UnsupportedOperationException("Implementation pending");
    }
}
