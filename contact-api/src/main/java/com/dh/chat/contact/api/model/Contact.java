package com.dh.chat.contact.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author marvin tola
 */
public interface Contact extends Serializable {

    Long getContactId();

    Long getUserId();

    Long getAccountId();

    String getEmail();

    String getName();

    String getAvatarId();

    Date getCreatedDate();
}
