package com.dh.chat.contact.api.input;

/**
 * @author marvin tola
 */
public class ContactSearchInput {

    private String information;

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }
}
